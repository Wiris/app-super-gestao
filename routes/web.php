<?php

use Illuminate\Support\Facades\Route;

/* Route::get('/', function () {
    return view('welcome');
});
 */

Route::get('/', function () {
    return redirect()->route('site.index');
});

Route::get("/home", [\App\Http\Controllers\PrincipalController::class, 'principal'])->name('site.index');
Route::get("/sobre-nos", [\App\Http\Controllers\SobreNosController::class, 'principal'])->name('site.sobre');
Route::get("/contato", [\App\Http\Controllers\ContatoController::class, 'principal'])->name('site.contato');

Route::get("/login", function () {
    return "login";
})->name('site.login');

Route::prefix('/app')->group(function () {
    Route::get("/clientes", function () {
        return "clientes";
    })->name('app.clientes');
    Route::get("/fornecedores", [\App\Http\Controllers\FornecedorController::class, 'principal'])->name('app.fornecedores');
    Route::get("/produtos", function () {
        return "produtos";
    })->name('app.produtos');
});

Route::get("/teste/{param1}/{param2}", [\App\Http\Controllers\TesteController::class, 'principal'])->name('site.teste');

Route::fallback(function () {
    echo "<h3>A rota acessada não existe. <a href=" . route('site.index') . ">Clique aqui!</a> para retornar ao inicio</h3>";
});
