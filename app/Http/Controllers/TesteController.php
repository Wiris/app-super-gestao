<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TesteController extends Controller
{
    public function principal(int $param1, int $param2)
    {
        // echo "A soma é: " . ($param1 + $param2);

        /* Usando Array associativo */
        // return view('site.teste', ["x" => $param1, "y" => $param2]);

        /* Usando compact */
        // return view('site.teste', compact("param1", "param2"));

        /* Usando With */
        return view('site.teste')->with('param1', $param1)->with('param2', $param2);
    }
}
